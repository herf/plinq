import typing


class Person(object):
    def __init__(self, name: str, age: int, phone_numbers: typing.List[int]=None) -> None:
        self.name = name
        self.age = age
        self.phone_numbers = phone_numbers
        if self.phone_numbers is None:
            self.phone_numbers = []

    def __eq__(self, other: "Person") -> bool:
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)

    def __str__(self):
        return "Name: {}, age: {}".format(self.name, self.age)


class House(object):
    def __init__(self, address: str, owner: str) -> None:
        self.address = address
        self.owner = owner


class FakeIterator:
    def __init__(self):
        pass


class ValidIterator:
    def __init__(self):
        pass

    def __iter__(self):
        pass

    def __next__(self):
        pass


class NoneIteratorIterable:
    def __init__(self):
        pass

    def __iter__(self):
        return None


class IntIteratorIterable:
    def __init__(self):
        pass

    def __iter__(self):
        return 5


class FakeIteratorIterable:
    def __init__(self):
        pass

    def __iter__(self):
        return FakeIterator()


class ValidIteratorIterable:
    def __init__(self):
        pass

    def __iter__(self):
        return ValidIterator()
